#! /usr/bin/perl 

# Copyright 2016 Ansgar Gruber
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# FASTA_reformat.pl re-writes the content of a fasta file (usage: perl FASTA_reformat.pl
# your_filename). Removes whitespace from beginnings and ends of lines,
# ignores comment (";") lines. Prints sequences in upper case without "-"
# or whitespace characters at a fixed width of 60 characters. Retains the
# order of sequences and does not filter out redundant sequences. The
# maximum number of sequences in the input file is 99999. Runs with
# Perl v5.18.2 on Darwin (and possibly with other Perl installations).

use warnings;
use strict;

# hardwired variables (find with text search for value):
# lenght of printed sequence line, follows FASTA recommendation: 60
# maximum number of sequences processed by FASTA_reformat.pl: 99999 (limit due to
# string length of unique id)

# name of fasta file
my $infile_fn;
if ( scalar(@ARGV) == 1 ) {
    $infile_fn = shift(@ARGV);
}
else {
    print(
        "Please type the name of the file that shall be re-written.\n"
    );
    $infile_fn = <stdin>;
}
chomp($infile_fn);
unwhite($infile_fn);    # remove any remaining whitespace

# load sequences from file, rename and format sequences
open( my $fasta_fh, "<", $infile_fn )
  or die( "FASTA_reformat.pl is unable to open " . $infile_fn . ", please check.\n" );
my $file_name = "sequence name";    # names in $fasta_fh file
my $file_seq  = "gatc";                 # sequences in $fasta_fh file
my %fasta =
  ();    # hash with sequences from file, names as keys and sequences as values
my %fasta_order = ()
  ; # hash with sequence names from file, number as key and sequence name as value
my $n = 0;    # number of sequences

while ( my $inputline = <$fasta_fh> ) {
    chomp($inputline);
    unwhite($inputline);      # remove any remaining whitespace
    my $firstCh = substr( $inputline, 0, 1 );
    unless ( $firstCh eq ";" ) {    # ignore comment lines
        if ( $firstCh eq ">" ) {    # new sequence identifier
            ( $file_name, $file_seq ) = mod( $file_name, $file_seq );
            $fasta{$file_name} = $file_seq;
            $fasta_order{$n}   = $file_name;
            $file_seq          = ();
            $file_name = substr( $inputline, 1 );
            $n++;
        }
        else {
            $file_seq .= $inputline;
        }
    }
}
( $file_name, $file_seq ) = mod( $file_name, $file_seq ); # add last sequence
$fasta{$file_name} = $file_seq;
$fasta_order{$n}   = $file_name;
close($fasta_fh);
delete $fasta{"sequence name"};
delete $fasta_order{0}
  ; # get rid of initial pair, which is needed to avoid undefined hashkey at first iteration of while loop

# count sequences, die and warn if too many
if ( scalar( keys(%fasta) ) > 99999 ) {
    die(    "The number of sequences in "
          . $infile_fn . " ("
          . scalar( keys(%fasta) )
          . ") is larger than the maximum number of sequences 'FASTA_reformat.pl' can process (99999)."
    );
}

# print sequences into new files
open( my $out_fh, ">", $infile_fn . "_reformatted.fas" ); # next output file
my $np=1;
while ( $np <= $n )
{    # print until all seqs are printed
    print $out_fh (
        ">" . $fasta_order{$np} . "\n" . $fasta{ $fasta_order{$np} } );
    $np++;
}
close($out_fh);


# say goodbye
print(  "\n"
      . $n
      . " sequences from "
      . $infile_fn
      . " reformatted and  written to "
      . $infile_fn . "_reformatted.fas\n" );
print(
"\nThank you for using FASTA_reformat.pl, please contact ansgar.gruber\@uni-konstanz.de \nwith any question concerning the program :-\)\n"
);

sub unwhite
{ # removes any whitespace (including linebreaks) from the beginning and the end of the scalar value passed to it
    unless ( scalar(@_) == 1 ) {
        die(
"\nSubroutine unwhite needs exactly one element of \@\_ (from wich any whitespace that might occur at the beginning or end will be removed), program died, please check.\n"
        );
    }
    else {
        $_[0] =~ s/^\s+//g;
        $_[0] =~ s/\s+$//g;
        return ( $_[0] );
    }
}    # end of sub unwhite

sub mod
{ # modifies sequence, deletes "-" and whitespace, converts to upper case, inserts linebreaks each 60 characters
    unless ( scalar(@_) == 2 ) {
        die(
"\nSubroutine mod needs exactly two elements of \@\_, program died, please check.\n"
        );
    }
    my $name = shift(@_);
    my $seq  = shift(@_);
    $seq =~ s/-+//g;                          # remove gaps ("-") from sequence
    $seq =~ s/\s+//g;                         # remove whitespace from sequence
    $seq = uc( $seq )
      ; # convert to upper case
    $seq =~ s/(.{80})/$1\n/g
      ;    # insert linebreak after each complete block of 80 characters

    unless ( substr( $seq, -1, 1 ) eq "\n" )
    {      # add linebreak to end of sequence
        $seq .= "\n";
    }
    return ( $name, $seq );
}    # end of sub mod